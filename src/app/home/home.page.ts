import { Component,OnInit } from '@angular/core';
import * as math from 'mathjs';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  raiz:any;
  xi:any;
  xu:any;
  iteraciones:any;
  real = 1;
  ultimoXr = 0;
  arrayMosy:any = [];
  constructor() {}

  ngOnInit() {
  }

  calcular(){
    
    if(this.xi != undefined && this.xu != undefined && this.raiz != undefined){
    this.arrayMosy = []
    console.log(this.raiz)
    console.log(this.xu)

    let xi = this.xi
    let xu = this.xu
    let palabra = this.raiz.indexOf("coss")
    if(palabra == -1){
    for(let i=0;i<this.iteraciones;i++){
      console.log(xi)
      let scope = {x:xi}
      let Fxi = math.eval(this.raiz,scope);
    //  let rs = math.sqrt(this.xi) para la raiz cuadrada
      console.log(Fxi)
      let scope2 = {x:xu}
      let Fxu = math.eval(this.raiz,scope2);  
      let result3 = xu-((Fxu*(xi-xu))/(Fxi-Fxu))
      
      console.log(result3)  
      let et = ((result3-this.real)/this.real)*100
      let ea = ((result3-this.ultimoXr)/result3)*100
      
      this.ultimoXr = result3
      let iteracion = i
      this.arrayMosy.push({iteracion:iteracion,xr:this.ultimoXr.toFixed(2),xu:xu.toFixed(2),xi:xi.toFixed(2),ea:math.abs(ea).toFixed(2)})
      xi = result3
      console.log(ea)
    }
    }else{
      console.log("que lindo coseno")
      let scope2 = {x:xu}
      let Fxu = math.eval(this.raiz,scope2);  
      console.log(Fxu)
    }

    console.log(this.arrayMosy)
    }else{
      console.log("malo")
    }

  }
  
}
