import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Metodo3Page } from './metodo3.page';

describe('Metodo3Page', () => {
  let component: Metodo3Page;
  let fixture: ComponentFixture<Metodo3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Metodo3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Metodo3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
